module.exports = function(params){
	var appObject = params.app;
	var connection = appObject.connection;
	var q = require('q');
	var operations = {};

	operations.readAllUniversities = function (req, res, next){
	// connection.connect();

		connection.query('SELECT * FROM university_info', function(err, rows, fields){
			if(!err){
				console.log("Solution:", rows);
				req.params.universityDetails = rows;
				req.crudStatus = 'success';
				next();
			}
			 else{
				console.log("Error:", err);
				req.crudStatus = err;
				next();
			}
		});
	};

	operations.insertUniversity = function(req, res, next){
		if(req.validated == true){
			var university = {
				uid : req.body.uid ,
				name : req.body.name ,
				location : req.body.location , 
			};

			connection.query('INSERT INTO university_info SET ?', university , function(err, res){
				if(!err){
					console.log("Inserted Id:", res.insertId);
					req.crudStatus = 'success';
					next();
				}
				else{
					console.log("Error:", err);
					req.crudStatus = err;
					next();
				}
			});
		}	
		else{
			req.crudStatus = 'Invalid input';
			next();
		}

	};

	operations.bulkInsert = function(req, res, next){
		for(var i = 0 ; i<req.body.bulkObject.length; i++){
			operations.insertRecords(req.body.bulkObject.uid, req.body.bulkObject.name, req.body.bulkObject.location);
		}

	};

	operations.updateUniversity = function(req, res, next){
		var updateArray = [req.body.name, req.body.location, req.body.uid];
		connection.query('UPDATE university_info SET name = ?, location = ? WHERE uid = ?', updateArray , function(err, res){
			if(!err){
				console.log('Item updated');
				req.crudStatus = 'success';
				next();
			}
			else{
				console.log("Error:", err);
				req.crudStatus = err;
				next();
			}
		});

	};

	operations.deleteUniversity = function(req, res, next){
		connection.query('DELETE FROM university_info WHERE uid = ?', req.body.uid , function(err, res){
			if(!err){
				console.log('Item deleted');
				req.crudStatus = 'success';
				next();
			}
			else{
				console.log("Error:", err);
				req.crudStatus = err;
				next();
			}
		});

	};

	operations.createUniversity = function(req, res, next){

		var creationQuery = "CREATE TABLE `university_info` (`uid` int(10) NOT NULL AUTO_INCREMENT, `name` varchar(255) NOT NULL, `location` varchar(255) NOT NULL, PRIMARY KEY (`uid`)) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;";

		connection.query(creationQuery, function(err, rows, fields){
			if(!err){
				console.log("Created table 'university_info'");
				next();
			}
			else{
				console.log("Error:", err);
				next();
			}
		});

	};


	operations.dropTable = function(req, res, next){

		var deletionQuery = "DROP TABLE "+req.body.tablename;

		connection.query(deletionQuery, function(err, rows, fields){
			if(!err){
				// console.log(rows);
				console.log("Deleted table", req.body.tablename);
				next();
			}
			else{
				console.log("Error:", err);
				next();
			}
		});
		
	};

	// createTable();
	// insertRecords();
	// viewAllRecords();
	// updateRecords();
	// viewAllRecords();
	// dropTable("employees");

	return operations;

};