module.exports = function(params){
	var express = require('express');
	var router = express.Router();
	var appObject = params.appObject;
	// var sqlOperations = require('./crud')({app : appObject});

	// get all players data
	router.get('/all',
		function(req, res, next){
			if(req){
				var playerJSON = require("./data.json");
				res.json(playerJSON);
			}
			else{
				res.status(500);
				res.send("Invalid request !");
			}
	});

	// Get player specific data
	router.get('/:player',
		function(req, res, next){
			if(req){
				var playerJSON = require("./data.json");
				playerJSON.forEach(function(singlePlayer){
					if(singlePlayer.ign == req.params.player){
						res.json(singlePlayer)
					}
				});
			}
			else{
				res.status(500);
				res.send("Invalid request !");
			}
	});

	// Update player data
	router.post('/:player',
		function(req, res, next){
			if(req.body){

				var playerJSON = require("./data.json");
				playerJSON.forEach(function(singlePlayer){
					if(singlePlayer.ign == req.params.player){
						singlePlayer.soloMMR = req.body.soloMMR;
						singlePlayer.partyMMR = req.body.partyMMR;
						singlePlayer.roles = req.body.roles;

						var fs = require("fs");
						fs.writeFile( __dirname + "/data.json", JSON.stringify(playerJSON), "utf8",function(){} );
						res.send("player updated");
					}
				});

				// var dataJson = require("./data.json");

			}
			else{
				res.status(500);
				res.send("Invalid request !");
			}
	});


	// Return routes for current module to centralized router.
	return router;

};
