module.exports = function(params) {

	var appObject = params.app;
	var express = require('express');
	var router = express.Router();

	// common stuff to do for any route
	// Global level settings for all incoming routes
	router.all('*', function(req, res, next) {
		console.log("Incoming route : " + req.url);
		next();
	});


	appObject.use(router);
	// appObject.use('/players', require(__dirname + '/user_modules/players/routes.js')({appObject : appObject}));
	// appObject.use('/teams', require(__dirname + '/user_modules/teams/routes.js')({appObject : appObject}));
	// appObject.use('/reports', require(__dirname + '/user_modules/reports/routes.js')({appObject : appObject}));

	return router;
};
