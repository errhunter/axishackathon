var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var colors = require('colors');

// HTTP Listening
var port = 8082;
var server_HTTP_Instance = app.listen(port);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

server_HTTP_Instance.on('error', function(error) {
	if (error.syscall !== 'listen') {
		throw error;
	} else { console.error(error); }
});
server_HTTP_Instance.on('listening', function() {
	console.log(('Axis Hackathon Challenge 2 app listening on port ' + port).bgBlack.cyan);
});

// Mogo init
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/axishk');
var mongooseConnection = mongoose.connection;
mongooseConnection.on('error', console.error.bind(console, 'MongoConnectionError : '));

mongooseConnection.once('open', function(callback) {
	console.log('MongoDB connected !'.blue);
});

// Security best practices
app.disable('x-powered-by');

// Public Folder binding
app.use(express.static(__dirname + "/public"));

// Centralized router
require('./routes')({ app: app });

app.use(function(req, res) {
	// Use res.sendfile, as it streams instead of reading the file into memory.
	res.sendFile(__dirname + '/public/index.html');
});

