'use strict';

angular.module('DotaOrganizer', [
	'ui.router',
	'Players',
	'Teams' ,
	'Reports'
	])

.config(function($stateProvider, $urlRouterProvider, $locationProvider){


	$stateProvider

		.state('home', {
			url : '/',
			templateUrl : 'app/home.html',
			controller : 'homeController'
		})
		.state('players', {
			url : 'players',
			templateUrl : 'app/players.html',
			controller : 'playersController'
		})
		.state('teams', {
			url : 'teams',
			templateUrl : 'app/teams.html',
			controller : 'teamsController'
		})
		.state('reports', {
			url : 'reports',
			templateUrl : 'app/reports.html',
			controller : 'reportsController'
		});

	$locationProvider.html5Mode(true);

})
.controller('universalController', function($scope, $http , $rootScope, $anchorScroll, $location, $state ,$stateParams, streamPortal, standardPortal, substreamPortal, paperPortal){

});
