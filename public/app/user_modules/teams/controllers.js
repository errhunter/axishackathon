angular.module('University')

.controller('universityCtrl', function($scope, $rootScope, $http){
	var domainName = 'localhost:8082';
	$scope.editMode = false;

	$scope.toggleEditMode =function(){
		$scope.editMode = !$scope.editMode;
	};

	// $scope.universities = [];

	$scope.getConfirmation = function(uid){

		var returnVal = confirm("Are you sure you want to delete ");
		if(returnVal){
			$scope.deleteUniversity(uid);
		}
	};

	var insertRecord = function(data, callbackFnS, callbackFnE){

		$http.post('/universities/create', data)
			.success(callbackFnS)
			.error(callbackFnE);
	};

	var viewRecord = function(data, callbackFnS, callbackFnE){

		$http.post('/universities/read', data)
			.success(callbackFnS)
			.error(callbackFnE);
	};

	$scope.viewUniversities = function(){

		viewRecord({}, 
			function(data, status, headers, config){
				$scope.universities = data;
				console.log(data);
			},
			 function(data, status, headers, config){
				console.log('Error');
			});

	};
	$scope.viewUniversities();

	$scope.createUniversity = function(){

		var newUniversity = {
			name : $scope.university.name,
			location : $scope.university.location,
		};

		insertRecord(newUniversity, 
			function(data, status, headers, config){
				console.log(data);
				$scope.universities.push(newUniversity);
				$scope.university = null;
				$scope.toggleEditMode();
				$scope.viewUniversities();
			},
			 function(data, status, headers, config){
				console.log('Error');
			});

	};

})

.controller('universityCardCtrl', function($scope, $http){
	
	$scope.editMode = false;

	$scope.toggleEditMode =function(){
		$scope.editMode = !$scope.editMode;
	};

	$scope.getConfirmation = function(uid){

		var returnVal = confirm("Are you sure you want to delete?");
		if(returnVal){
			$scope.deleteUniversity(uid);
		}
	};

	var updateRecord = function(data, callbackFnS, callbackFnE){

		$http.post('/universities/update', data)
			.success(callbackFnS)
			.error(callbackFnE);
	};

	var deleteRecord = function(data, callbackFnS, callbackFnE){

		$http.post('/universities/delete', data)
			.success(callbackFnS)
			.error(callbackFnE);
	};

	$scope.updateUniversity = function(){
		var updateObject = {
			name: $scope.item.name, 
			location: $scope.item.location, 
			uid: $scope.item.uid};
		updateRecord(updateObject, 
			function(data, status, headers, config){
				console.log(data);
				$scope.toggleEditMode();
				$scope.viewUniversities();
			},
			 function(data, status, headers, config){
				console.log('Error');
			});

	};

	$scope.deleteUniversity = function(){
		
		var deleteObject = {
			uid : $scope.item.uid
		}
		
		deleteRecord(deleteObject, 
			function(data, status, headers, config){
				console.log(data);
				$scope.viewUniversities();
			},
			 function(data, status, headers, config){
				console.log('Error');
			});

	};

});