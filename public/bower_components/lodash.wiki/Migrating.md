## Underscore

When migrating from Underscore to Lodash there are some differences to be aware of.

* Underscore `_.clone` copies inherited `object` properties
* Underscore `_.contains` is Lodash’s `_.includes`
* Underscore `_.each` doesn’t allow exiting by returning `false`
* Underscore `_.flatten` is deep by default while Lodash’s is shallow
* Underscore `_.isFinite` doesn’t align with ES6 `Number.isFinite`<br>
  (e.g. `_.isFinite('1')` returns `true` in Underscore but `false` in Lodash)
* Underscore `_.matches` shorthand doesn’t support deep comparisons<br>
  (e.g. `_.filter(objects, { 'a': { 'b': 'c' } })`)
* Underscore 1.6 & 1.8 `_.defaults` & `_.extend` iterate over inherited source properties
* Underscore 1.7 `_.extend` aligned with Lodash
* Underscore ≥ 1.7 & Lodash have changed their `_.template` syntax to<br>`_.template(string, option)(data)`
* Underscore 1.8 `_.mapObject` is implemented in Lodash as `_.mapValues`
* Lodash mimics the interface of ES6 `Map` for memoize caches
* Lodash supports [implicit chaining](https://lodash.com/docs#_), [lazy chaining, & shortcut fusion](http://filimanjaro.com/blog/2014/introducing-lazy-evaluation/)
* Lodash split its overloaded `_.head`, `_.last`, `_.rest`, & `_.initial` out into<br>
  `_.take`, `_.takeRight`, `_.drop`, & `_.dropRight`<br>
  (i.e. `_.head(array, 2)` in Underscore is `_.take(array, 2)` in Lodash)

## Backbone

Lodash works great with Backbone. It’s even run against Backone’s unit tests on every commit.<br>
You can replace Underscore with Lodash in Backbone by using an [AMD loader](http://requirejs.org/), [browserify](http://browserify.org/), or [webpack](http://webpack.github.io/).

**Note:** Lodash v4 works with Backbone ≥ v1.3.0.

 * Using AMD “paths” configuration
  ```js
  require({
    'paths' {
      'backbone': 'path/to/backbone',
      'jquery': 'path/to/jquery',
      'underscore': 'path/to/lodash'
    }
  }, ['backbone'], function(Backbone) {
    // use Backbone
  });
  ```

 * Using the [aliasify](https://www.npmjs.com/package/aliasify) transform for browserify by adding a section to your package.json
  ```json
  {
    "browserify": {
      "transform": ["aliasify"]
    },
    "aliasify": {
      "aliases": {
        "underscore": "lodash"
      }
    }
  }
  ```

  and executing browserify with the `aliasify` parameter
  ```bash
  $ browserify entry.js --global-transform aliasify -o out.js
  ```

 * Using the [resolve.alias](https://webpack.github.io/docs/configuration.html#resolve-alias) configuration in webpack
  ```js
  module.exports = {
    'resolve': {
      'alias': {
        'underscore': 'absolute/path/to/lodash'
      }
    }
  };
  ```
