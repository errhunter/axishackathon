## lodash/fp

The `lodash/fp` module is an instance of `lodash` with its methods wrapped to
produce immutable auto-curried iteratee-first data-last methods.

## Installation

In a browser:
```html
<script src='path/to/lodash.js'></script>
<script src='path/to/lodash.fp.js'></script>
```

In Node.js:
```js
// Load the fp build.
var _ = require('lodash/fp');

// Load a method category.
var object = require('lodash/fp/object');

// Load a single method for smaller builds with browserify/rollup/webpack.
var extend = require('lodash/fp/extend');
```

## Convert

Although `lodash/fp` & its method modules come pre-converted, there are times when
you may want to convert another lodash package or create a customized conversion.
That’s when the `convert` module comes in handy.

```js
var convert = require('lodash/fp/convert');

// Convert by name.
var assign = convert('assign', require('lodash.assign'));

// Convert by object.
var fp = convert({
  'assign': require('lodash.assign'),
  'chunk': require('lodash.chunk')
});

// Convert by `lodash` instance.
var fp = convert(lodash.runInContext());
```

It’s even customizable.
```js
// Every option is `true` by default.
var filter = convert('filter', _.filter, {
  // Specify capping iteratee arguments.
  'cap': true,
  // Specify currying.
  'curry': true,
  // Specify fixed arity.
  'fixed': true,
  // Specify immutable operations.
  'immutable': true,
  // Specify rearranging arguments.
  'rearg': true
});

// Specify `cap` of `false` to create a function that doesn’t cap iteratee arguments.
var filter = convert('filter', _.filter, { 'cap': false });

filter(function(value, index) {
  return index % 2 == 0;
})(['a', 'b', 'c']);
// => ['a', 'c']
```

## Mapping

Immutable auto-curried iteratee-first data-last methods sound great, but what
does that really mean for each method? Below is a breakdown of the mapping used
to convert each method.

#### Capped Iteratee Arguments

Methods that cap iteratees to one argument:<br>
`cloneDeepWith`, `cloneWith`, `dropRightWhile`, `dropWhile`, `every`,
`filter`, `find`, `findIndex`, `findKey`, `findLast`,
`findLastIndex`, `findLastKey`, `flatMap`, `forEach`, `forEachRight`,
`forIn`, `forInRight`, `forOwn`, `forOwnRight`, `map`,
`mapKeys`, `mapValues`, `partition`, `reject`, `remove`,
`some`, `takeRightWhile`, `takeWhile`, & `times`

Methods that cap iteratees to two arguments:<br>
`assignInWith`, `assignWith`, `isEqualWith`, `isMatchWith`, `reduce`,
`reduceRight`, & `transform`

The iteratee of `mapKeys` is invoked with one argument: (key)

#### Fixed Arity

Methods with a fixed arity of one:<br>
`attempt`, `castArray`, `ceil`, `create`, `curry`,
`curryRight`, `floor`, `fromPairs`, `invert`, `iteratee`,
`memoize`, `method`, `methodOf`, `mixin`, `over`,
`overEvery`, `overSome`, `rest`, `reverse`, `round`,
`runInContext`, `spread`, `template`, `trim`, `trimEnd`,
`trimStart`, `uniqueId`, & `words`

Methods with a fixed arity of two:<br>
`add`, `after`, `ary`, `assign`, `assignIn`,
`at`, `before`, `bind`, `bindKey`, `chunk`,
`cloneDeepWith`, `cloneWith`, `concat`, `countBy`, `curryN`,
`curryRightN`, `debounce`, `defaults`, `defaultsDeep`, `delay`,
`difference`, `drop`, `dropRight`, `dropRightWhile`, `dropWhile`,
`endsWith`, `eq`, `every`, `filter`, `find`,
`find`, `findIndex`, `findKey`, `findLast`, `findLastIndex`,
`findLastKey`, `flatMap`, `flattenDepth`, `forEach`, `forEachRight`,
`forIn`, `forInRight`, `forOwn`, `forOwnRight`, `get`,
`groupBy`, `gt`, `gte`, `has`, `hasIn`,
`includes`, `indexOf`, `intersection`, `invertBy`, `invoke`,
`invokeMap`, `isEqual`, `isMatch`, `join`, `keyBy`,
`lastIndexOf`, `lt`, `lte`, `map`, `mapKeys`,
`mapValues`, `matchesProperty`, `maxBy`, `merge`, `minBy`,
`omit`, `omitBy`, `overArgs`, `pad`, `padEnd`,
`padStart`, `parseInt`, `partial`, `partialRight`, `partition`,
`pick`, `pickBy`, `pull`, `pullAll`, `pullAt`,
`random`, `range`, `rangeRight`, `rearg`, `reject`,
`remove`, `repeat`, `result`, `sampleSize`, `some`,
`sortBy`, `sortedIndex`, `sortedIndexOf`, `sortedLastIndex`, `sortedLastIndexOf`,
`sortedUniqBy`, `split`, `startsWith`, `subtract`, `sumBy`,
`take`, `takeRight`, `takeRightWhile`, `takeWhile`, `tap`,
`throttle`, `thru`, `times`, `trimChars`, `trimCharsEnd`,
`trimCharsStart`, `truncate`, `union`, `uniqBy`, `uniqWith`,
`unset`, `unzipWith`, `without`, `wrap`, `xor`,
`zip`, `zipObject`, & `zipObjectDeep`

Methods with a fixed arity of three:<br>
`assignInWith`, `assignWith`, `clamp`, `differenceBy`, `differenceWith`,
`getOr`, `inRange`, `intersectionBy`, `intersectionWith`, `isEqualWith`,
`isMatchWith`, `mergeWith`, `orderBy`, `pullAllBy`, `pullAllWith`,
`reduce`, `reduceRight`, `replace`, `set`, `slice`,
`sortedIndexBy`, `sortedLastIndexBy`, `transform`, `unionBy`, `unionWith`,
`update`, `xorBy`, `xorWith`, & `zipWith`

Methods with a fixed arity of four:<br>
`fill`, `setWith`, & `updateWith`

#### Rearranged Arguments

Methods with a fixed arity of two have an argument order of:<br>
`(b, a)`

Methods with a fixed arity of three have an argument order of:<br>
`(b, c, a)`

Methods with a fixed arity of four have an argument order of:<br>
`(c, d, b, a)`

Methods with custom argument orders:<br>
 * `_.assignInWith` has an order of `(c, a, b)`
 * `_.assignWith` has an order of `(c, a, b)`
 * `_.getOr` has an order of `(c, b, a)`
 * `_.isMatchWith` has an order of `(c, b, a)`
 * `_.mergeWith` has an order of `(c, a, b)`
 * `_.pullAllBy` has an order of `(c, b, a)`
 * `_.pullAllWith` has an order of `(c, b, a)`
 * `_.setWith` has an order of `(d, b, c, a)`
 * `_.sortedIndexBy` has an order of `(c, b, a)`
 * `_.sortedLastIndexBy` has an order of `(c, b, a)`
 * `_.updateWith` has an order of `(d, b, c, a)`
 * `_.zipWith` has an order of `(c, a, b)`

Methods with unchanged argument orders:<br>
`add`, `assign`, `assignIn`, `concat`, `difference`,
`gt`, `gte`, `lt`, `lte`, `matchesProperty`,
`merge`, `partial`, `partialRight`, `random`, `range`,
`rangeRight`, `subtract`, `zip`, & `zipObject`

The methods `partial` & `partialRight` accept an array of arguments to partially
apply as their second parameter.

#### New Methods

Methods created to accommodate Lodash’s variadic methods:<br>
`curryN`, `curryRightN`, `getOr`, `trimChars`, `trimCharsEnd`, &
`trimCharsStart`

#### Aliases

There are 34 method aliases:<br>
 * `_.__` is an alias of `_.placeholder`
 * `_.all` is an alias of `_.some`
 * `_.allPass` is an alias of `_.overEvery`
 * `_.apply` is an alias of `_.spread`
 * `_.assoc` is an alias of `_.set`
 * `_.assocPath` is an alias of `_.set`
 * `_.compose` is an alias of `_.flowRight`
 * `_.contains` is an alias of `_.includes`
 * `_.dissoc` is an alias of `_.unset`
 * `_.dissocPath` is an alias of `_.unset`
 * `_.each` is an alias of `_.forEach`
 * `_.eachRight` is an alias of `_.forEachRight`
 * `_.equals` is an alias of `_.isEqual`
 * `_.extend` is an alias of `_.assignIn`
 * `_.extendWith` is an alias of `_.assignInWith`
 * `_.first` is an alias of `_.head`
 * `_.init` is an alias of `_.initial`
 * `_.mapObj` is an alias of `_.mapValues`
 * `_.omitAll` is an alias of `_.omit`
 * `_.nAry` is an alias of `_.ary`
 * `_.path` is an alias of `_.get`
 * `_.pathEq` is an alias of `_.matchesProperty`
 * `_.pathOr` is an alias of `_.getOr`
 * `_.pickAll` is an alias of `_.pick`
 * `_.pipe` is an alias of `_.flow`
 * `_.prop` is an alias of `_.get`
 * `_.propOf` is an alias of `_.propertyOf`
 * `_.propOr` is an alias of `_.getOr`
 * `_.somePass` is an alias of `_.overSome`
 * `_.unapply` is an alias of `_.rest`
 * `_.unnest` is an alias of `_.flatten`
 * `_.useWith` is an alias of `_.overArgs`
 * `_.whereEq` is an alias of `_.filter`
 * `_.zipObj` is an alias of `_.zipObject`
